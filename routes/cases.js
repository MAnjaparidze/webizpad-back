const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createCase, getCases, getCase, updateCaseAlias } = require('../models/case');

router.post("/", async (req, res) => {
    let result = await getCases(req.body);
    return res.send(result);
})

router.post('/add', async (req, res) => {
    let result = await createCase(req.body);
    return res.send(result);
})

router.post("/get", async (req, res) => {
    let result = await getCase(req.body.id);
    return res.send(result);
})

router.post("/update", async(req,res) => {
    let result = await updateCaseAlias(req.body);
    return res.send(result);
})

module.exports = router;