const _ = require('lodash');
const auth = require('../middleware/auth');
const jwt = require('jsonwebtoken');
const config = require('config');
const { User, activateAccount, forgotPassword, verifyToken, resetPassword, createUser, updateUserInfo } = require('../models/user');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MAILGUN
const mailgun = require("mailgun-js");
const DOMAIN = 'sandboxda2224b9858d4011ab1957ffb9895e26.mailgun.org';
const mg = mailgun({ apiKey: process.env.MAILGUN_APIKEY, domain: DOMAIN });


router.get("/me", async (req, res) => {
    const user = User.findById(req.user._id).select("-password");
    res.send(user)
});

router.post('/', async (req, res) => {
    let result = await createUser(req, res);
    return result;
});

router.put("/update", async (req,res) => {
    let result = await updateUserInfo(req.body, res);
    console.log(result);
    return res.send(result);
});

router.post("/activate-account", async (req, res) => {
    let result = await activateAccount(req, res);
    return res.send(result);
})

router.put('/forgot-password', async (req, res) => {
    let result = await forgotPassword(req, res);
    return res.send(result);
})

router.put('/reset-password', async (req, res) => {
    let result = await resetPassword(req, res);
    return res.send(result);
})

router.put("/verify-token", async (req, res) => {
    await verifyToken(req, res);
})


module.exports = router;