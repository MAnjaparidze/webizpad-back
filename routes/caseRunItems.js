const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createCaseRunItem, getCaseRunItems, getCaseRunItem, updateCaseRunItem } = require('../models/caseRunItem');

router.post("/", async (req, res) => {
    let result = await getCaseRunItems(req.body);
    return res.send(result);
})

router.post('/add', async (req, res) => {
    let result = await createCaseRunItem(req.body);
    return res.send(result);
})

router.post("/get", async (req, res) => {
    let result = await getCaseRunItem(req.body.id);
    return res.send(result);
})

router.post("/update", async (req, res) => {
    let result = await updateCaseRunItem(req.body);
    return res.send(result);
})

module.exports = router;