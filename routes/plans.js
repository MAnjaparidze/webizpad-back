const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createPlan, getPlans, getPlan } = require('../models/plan');

router.post("/", async (req, res) => {
    let result = await getPlans(req.body);
    return res.send(result);
})

router.post('/add', async (req,res) => {
    let result = await createPlan(req.body);
    return res.send(result);
})

router.post("/get", async (req, res) => {
    let result = await getPlan(req.body.id);
    return res.send(result);
})

module.exports = router;