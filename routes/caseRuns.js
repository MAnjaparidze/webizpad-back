const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createCaseRun, getCaseRuns, getCaseRun, updateCaseRun, deleteCaseRun } = require('../models/caseRun');

router.post("/", async (req, res) => {
    let result = await getCaseRuns(req.body);
    return res.send(result);
})

router.post('/add', async (req, res) => {
    let result = await createCaseRun(req.body);
    return res.send(result);
})

router.post("/get", async (req, res) => {
    let result = await getCaseRun(req.body.id);
    return res.send(result);
})

router.post("/update", async(req, res) => {
    let result = await updateCaseRun(req.body);
    return res.send(result);
})

router.post("/delete", async(req, res) => {
    let result = await deleteCaseRun(req.body);
    return res.send(result);
})

module.exports = router;