const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createCaseStep, getCaseSteps, getCaseStep } = require('../models/caseStep');

router.post("/", async (req, res) => {
    let result = await getCaseSteps(req.body);
    return res.send(result);
})

router.post('/add', async (req,res) => {
    let result = await createCaseStep(req.body);
    return res.send(result);
})

router.post("/get", async (req, res) => {
    let result = await getCaseStep(req.body.id);
    return res.send(result);
})

module.exports = router;