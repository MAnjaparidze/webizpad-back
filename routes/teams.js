const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { createTeam, getTeam, addTeamMember, addTeamProject } = require('../models/team');

router.post("/get", async (req, res) => {
    let result = await getTeam(req.body);
    return res.send(result);
});

router.post('/add', async (req, res) => {
    let result = await createTeam(req.body);
    return res.send(result);
});

router.put("/add_member", async(req,res) => {
    let result = await addTeamMember(req.body);
    return res.send(result);
});

router.put('/add_project', async(req,res) => {
    let result = await addTeamProject(req.body);
    return res.send(result);
})

module.exports = router;