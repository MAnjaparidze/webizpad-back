"use strict";

var auth = require('../middleware/auth');

var express = require('express');

var router = express.Router();

var _require = require('../models/team'),
    createTeam = _require.createTeam,
    getTeam = _require.getTeam,
    addTeamMember = _require.addTeamMember,
    addTeamProject = _require.addTeamProject;

router.post("/get", function _callee(req, res) {
  var result;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(getTeam(req.body));

        case 2:
          result = _context.sent;
          return _context.abrupt("return", res.send(result));

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.post('/add', function _callee2(req, res) {
  var result;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(createTeam(req.body));

        case 2:
          result = _context2.sent;
          return _context2.abrupt("return", res.send(result));

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
});
router.put("/add_member", function _callee3(req, res) {
  var result;
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(addTeamMember(req.body));

        case 2:
          result = _context3.sent;
          return _context3.abrupt("return", res.send(result));

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
});
router.put('/add_project', function _callee4(req, res) {
  var result;
  return regeneratorRuntime.async(function _callee4$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap(addTeamProject(req.body));

        case 2:
          result = _context4.sent;
          return _context4.abrupt("return", res.send(result));

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
});
module.exports = router;