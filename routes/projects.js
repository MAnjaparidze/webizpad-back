const auth = require('../middleware/auth');
const express = require('express');
const router = express.Router();

const { Project, createProject, getProject, getProjects } = require('../models/project');


router.post('/', async (req, res) => {
    const result = await getProjects(req.body);
    return res.send(result);
})

router.post('/add', async (req,res) => {
    let result = await createProject(req.body)
    return res.send(result);
})

router.post('/get', async(req, res) => {
    let result = await getProject(req.body.ProjectID);
    return res.send(result);
})


module.exports = router;