**Setup**

**Install MongoDB**
To run this project, you need to install the latest version of MongoDB Community Edition first along with MongoDB Compass.

https://www.mongodb.com/try/download/community

Once you install MongoDB, make sure it's running.

**Install the Dependencies**

npm i

**Environment Variables**
In the project I am using a property called jwtPrivateKey. This key is used to encrypt JSON web tokens. So, for security reasons, it should not be checked into the source control. To run the project you should export it before starting the node server.

export vidly_jwtPrivateKey=yourSecureKey

**Start the Server**
node or index.js
