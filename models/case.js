const mongoose = require('mongoose');
const { func } = require('joi');

const caseSchema = new mongoose.Schema({
    name: String,
    parentID: String,
    testNum: Number,
    passNum: Number,
    failNum: Number,
    queryNum: Number,
    blockedNum: Number,
    excludeNum: Number,
    testerAlias: Array,
    date: { type: Date, default: Date.now }
});

const Case = mongoose.model("Case", caseSchema);

async function createCase(caseItem) {
    const demoCase = new Case({
        name: caseItem.name,
        parentID: caseItem.parentID,
        testNum: caseItem.testNum,
        passNum: caseItem.passNum,
        failNum: caseItem.failNum,
        queryNum: caseItem.queryNum,
        blockedNum: caseItem.blockedNum,
        excludeNum: caseItem.excludeNum,
        testerAlias: caseItem.testerAlias
    });

    const result = await demoCase.save();
    const cases = await Case.find();
    return cases;
}

async function getCases(parent) {
    const cases = await Case.find()
        .then(res => {
            return res.filter(c => c.parentID === parent.parentID)
        })
    return cases;
} 

async function getCase(caseID) {
    const caseItem = await Case.findById(caseID);
    return caseItem;
}

async function updateCaseAlias(item) {
    const caseItem = Case.updateOne({ _id: item._id }, {
        $set: {
            testerAlias: item.testerAlias
        }
    })
    return caseItem;
}

exports.Case = Case;
exports.createCase = createCase;
exports.getCases = getCases;
exports.getCase = getCase;
exports.updateCaseAlias = updateCaseAlias;