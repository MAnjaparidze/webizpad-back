const mongoose = require('mongoose');
const { func } = require('joi');

const teamSchema = new mongoose.Schema({
    name: String,
    author: String,
    members: Array,
    projects: Array,
    date: { type: Date, default: Date.now }
});

const Team = mongoose.model("Team", teamSchema);

async function createTeam(teamObj) {
    const team = new Team({
        name: teamObj.name,
        author: teamObj.author,
        members: [teamObj.member],
        projects: [],
    });

    const result = await team.save();
    return result;
}

async function getTeam(teamObj) {
    const result = await Team.findOne({ name: teamObj.teamname });
    console.log(result);
    return result;
}

async function addTeamMember(teamObj) {
    const teamItem = Team.findOneAndUpdate({ _id: teamObj._id }, {
        $push: {
            members: teamObj.member
        }
    })
    return teamItem;
}

async function addTeamProject(teamObj) {
    const teamItem = Team.findOneAndUpdate({ _id: teamObj._id }, {
        $push: {
            projects: teamObj.project
        }
    })
    return teamItem;
}

exports.Team = Team;
exports.createTeam = createTeam;
exports.getTeam = getTeam;
exports.addTeamMember = addTeamMember;
exports.addTeamProject = addTeamProject;