const mongoose = require('mongoose');
const { func } = require('joi');

const { CaseRunItem } = require('./caseRunItem');

const caseRunSchema = new mongoose.Schema({
    inProgress: Boolean,
    parentCase: String,
    queueNumber: Number,
    testerAlias: Array,
    tagFilter: Array,
    build: String,
    date: String,
    time: String,
    passNum: Number,
    failNum: Number,
    blockNum: Number,
    queryNum: Number,
    totalNum: Number,
});

const CaseRun = mongoose.model("caserun", caseRunSchema);

async function createCaseRun(caseRun) {
    const queueNum = await generateCaseRunID(caseRun);
    let dateTime = generateCaseRunDate();

    const demoCaseRun = new CaseRun({
        inProgress: caseRun.inProgress,
        parentCase: caseRun.parentCase,
        queueNumber: queueNum,
        testerAlias: caseRun.testerAlias,
        tagFilter: caseRun.tagFilter,
        build: caseRun.build,
        date: dateTime.date,
        time: dateTime.time,
        passNum: caseRun.passNum,
        failNum: caseRun.failNum,
        blockNum: caseRun.blockNum,
        queryNum: caseRun.queryNum,
        totalNum: caseRun.totalNum,
    });

    const result = await demoCaseRun.save();
    return result;
}

async function getCaseRuns(caseItem) {
    const caseRuns = await CaseRun.find()
        .then(res => {
            return res.filter(c => c.parentCase === caseItem.id)
        })
    return caseRuns;
}

async function getCaseRun(caseRunID) {
    const caseRunItem = await CaseRun.findById(caseRunID);
    return caseRunItem;
}

async function updateCaseRun(caseRunObj) {
    const caseRun = await CaseRun.updateOne({ _id: caseRunObj._id }, {
        $set: {
            inProgress: caseRunObj.object.inProgress,
            testerAlias: caseRunObj.object.testerAlias,
            passNum: caseRunObj.object.passNum,
            failNum: caseRunObj.object.failNum,
            blockNum: caseRunObj.object.blockNum,
            queryNum: caseRunObj.object.queryNum,
            totalNum: caseRunObj.object.totalNum
        }
    })
    return caseRun;
}

async function deleteCaseRun(caseRunObj) {
    const result = await CaseRun.deleteOne({ _id: caseRunObj.id });
    const res = await CaseRunItem.deleteMany({ parentRun: caseRunObj.id })
    return result;
}

async function generateCaseRunID(caseItem) {
    const caseRuns = await CaseRun.find()
        .then(res => {
            return res.filter(c => c.parentCase === caseItem.parentCase)
        })
    if (caseRuns.length === 0) {
        return 1;
    } else {
        const lastItem = caseRuns[caseRuns.length - 1];
        return lastItem.queueNumber + 1;
    }
}

function generateCaseRunDate() {
    const date = new Date();
    const options = { year: 'numeric', month: 'short', day: 'numeric' };
    const resDate = date.toLocaleString('ge-GE', options);
    const result = { date: resDate, time: date.toTimeString() }
    return result;
}

exports.CaseRun = CaseRun;
exports.createCaseRun = createCaseRun;
exports.getCaseRuns = getCaseRuns;
exports.getCaseRun = getCaseRun;
exports.updateCaseRun = updateCaseRun;
exports.deleteCaseRun = deleteCaseRun;