const mongoose = require('mongoose');
const { func } = require('joi');

const caseStepSchema = new mongoose.Schema({
    name: String,
    parentCaseID: String,
    date: {type: Date, default: Date.now},
})

const TestCase = mongoose.model("CaseSteps", caseStepSchema);

async function createCaseStep(testCaseObject) {
    const demoTestCase = new TestCase({
        name: testCaseObject.name,
        parentCaseID: testCaseObject.parentCaseID,
        date: testCaseObject.date,
    });

    let result = await demoTestCase.save();
    // const testCases = await TestCase.find();
    return result;
}

async function getCaseSteps(caseObject) {
    const testCases = await TestCase.find()
        .then(res => {
            return res.filter(c => c.parentCaseID === caseObject.id)
        })
    return testCases;
}

async function getCaseStep(testCaseID) {
    const testCaseItem = await TestCase.findById(testCaseID);
    return testCaseItem;
}

exports.TestCase = TestCase;
exports.createCaseStep = createCaseStep;
exports.getCaseSteps = getCaseSteps;
exports.getCaseStep = getCaseStep;