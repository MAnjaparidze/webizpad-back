const mongoose = require('mongoose');


// Defining the Project and Creating a Class
const projectSchema = new mongoose.Schema({
    name: String,
    author: String,
    date: { type: Date, default: Date.now }
})

const Project = mongoose.model('Project', projectSchema);


async function createProject(project) {
    const demoProject = new Project({
        name: project.name,
        author: project.author,
    })
    const result = await demoProject.save();
    const projects = await Project.find();
    return projects;
}

async function getProjects(user) {
    const projects = await Project.find()
        .then(res => {
            return res.filter(c => c.author === user.userID)
        });
        return projects;
}

async function getProject(projectID) {
    const projectItem = await Project.findById(projectID);
    return projectItem;
}

exports.Project = Project;
exports.createProject = createProject;
exports.getProjects = getProjects;
exports.getProject = getProject;