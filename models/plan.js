const mongoose = require('mongoose');
const { func } = require('joi');

const planSchema = new mongoose.Schema({
    name: String,
    parentID: String,
    testNum: Number,
    passNum: Number,
    failNum: Number,
    queryNum: Number,
    blockedNum: Number,
    excludeNum: Number,
    date: { type: Date, default: Date.now }
});

const Plan = mongoose.model("Plan", planSchema);

async function createPlan(projectPlan) {
    const demoPlan = new Plan({
        name: projectPlan.name,
        parentID: projectPlan.parentID,
        testNum: projectPlan.testNum,
        passNum: projectPlan.passNum,
        failNum: projectPlan.failNum,
        queryNum: projectPlan.queryNum,
        blockedNum: projectPlan.blockedNum,
        excludeNum: projectPlan.excludeNum,
    });

    const result = await demoPlan.save();
    const plans = await Plan.find();
    return plans;
}

async function getPlans(project) {
    const plans = await Plan.find()
        .then(res => {
            return res.filter(c => c.parentID === project.parentID)
        })
    return plans;
}

async function getPlan(planID) {
    const planItem = await Plan.findById(planID);
    return planItem;
}

exports.Plan = Plan;
exports.createPlan = createPlan;
exports.getPlans = getPlans;
exports.getPlan = getPlan;