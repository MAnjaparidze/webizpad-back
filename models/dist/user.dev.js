"use strict";

var Joi = require('joi');

var mongoose = require('mongoose');

var config = require('config');

var jwt = require('jsonwebtoken');

var _ = require('lodash');

var bcrypt = require('bcrypt');

var _require = require('../models/team'),
    createTeam = _require.createTeam; // MAILGUN


var mailgun = require("mailgun-js");

var DOMAIN = 'sandboxda2224b9858d4011ab1957ffb9895e26.mailgun.org';
var mg = mailgun({
  apiKey: process.env.MAILGUN_APIKEY,
  domain: DOMAIN
});
var userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 100
  },
  firstname: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 250
  },
  lastname: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 250
  },
  email: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 64
  },
  teamname: {
    type: String,
    required: true,
    maxlength: 100
  },
  isActivated: {
    type: Boolean,
    required: true
  },
  role: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    "default": Date.now
  }
});

userSchema.methods.generateAuthToken = function () {
  var token = jwt.sign({
    _id: this._id,
    _email: this.email,
    _username: this.username,
    _firstname: this.firstname,
    _lastname: this.lastname,
    _team: this.teamname,
    _isActivated: this.isActivated,
    _role: this.role,
    _date: this.date
  }, config.get('jwtPrivateKey'));
  return token;
};

var User = mongoose.model('User', userSchema);

function createUser(req, res) {
  var _validateUser, error, user, salt, teamObj, _res, token, tokenAcc, data;

  return regeneratorRuntime.async(function createUser$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _validateUser = validateUser(req.body.user), error = _validateUser.error;

          if (!error) {
            _context.next = 3;
            break;
          }

          return _context.abrupt("return", res.status(400).send(error.details[0].message));

        case 3:
          _context.next = 5;
          return regeneratorRuntime.awrap(User.findOne({
            email: req.body.user.email
          }));

        case 5:
          user = _context.sent;

          if (!user) {
            _context.next = 8;
            break;
          }

          return _context.abrupt("return", res.status(400).send('User already registered.'));

        case 8:
          user = new User(_.pick(req.body.user, ['username', 'firstname', 'lastname', 'email', 'password', 'teamname', "isActivated", "role"]));
          _context.next = 11;
          return regeneratorRuntime.awrap(bcrypt.genSalt(10));

        case 11:
          salt = _context.sent;
          _context.next = 14;
          return regeneratorRuntime.awrap(bcrypt.hash(user.password, salt));

        case 14:
          user.password = _context.sent;
          _context.next = 17;
          return regeneratorRuntime.awrap(user.save());

        case 17:
          if (req.body.isTeamMember) {
            _context.next = 23;
            break;
          }

          teamObj = {
            name: user.teamname,
            author: user._id,
            member: {
              id: user._id,
              firstname: user.firstname,
              lastname: user.lastname,
              role: user.role,
              email: user.email,
              date: user.date
            }
          };
          console.log(teamObj);
          _context.next = 22;
          return regeneratorRuntime.awrap(createTeam(teamObj));

        case 22:
          _res = _context.sent;

        case 23:
          token = user.generateAuthToken();
          tokenAcc = jwt.sign({
            _id: user._id
          }, process.env.JWT_ACC_ACTIVATE, {
            expiresIn: "20m"
          });
          data = {
            from: 'webizpad-team@webizpad.com',
            to: req.body.user.email,
            subject: 'Account Activation Link',
            // text: 'Testing some Mailgun awesomness!'
            html: "\n        <h2>Please click on given link to activate your account</h2>\n        <a href=\"".concat(process.env.CLIENT_URL, "/authentication/activate/").concat(tokenAcc, "\">Click Here to Activate Your Account</a>\n        ")
          };
          mg.messages().send(data, function (error, body) {
            if (error) {
              return res.json({
                message: error.message
              });
            }

            res.header('x-auth-token', token).send(_.pick(user, ['_id', 'username', 'email']));
          });
          return _context.abrupt("return", {
            message: "Account has been created, activate it"
          });

        case 28:
        case "end":
          return _context.stop();
      }
    }
  });
}

function activateAccount(req, res) {
  var token = req.body.token;

  if (token) {
    return jwt.verify(token, process.env.JWT_ACC_ACTIVATE, function _callee(err, decodedToken) {
      var user, userObj;
      return regeneratorRuntime.async(function _callee$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!err) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return", res.send({
                status: 200,
                error: "Incorrect of Expired link."
              }));

            case 2:
              user = decodedToken;
              _context2.next = 5;
              return regeneratorRuntime.awrap(User.updateOne({
                _id: user._id
              }, {
                $set: {
                  isActivated: true
                }
              }));

            case 5:
              userObj = _context2.sent;
              return _context2.abrupt("return", res.send({
                status: 200,
                message: "Account activated, please Login"
              }));

            case 7:
            case "end":
              return _context2.stop();
          }
        }
      });
    });
  }
}

function forgotPassword(req) {
  var email, user, token, data, msg;
  return regeneratorRuntime.async(function forgotPassword$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          email = req.body.email;
          _context3.next = 3;
          return regeneratorRuntime.awrap(User.findOne({
            email: req.body.email
          }));

        case 3:
          user = _context3.sent;

          if (user) {
            _context3.next = 6;
            break;
          }

          return _context3.abrupt("return", {
            message: "User with this email does not exist."
          });

        case 6:
          token = jwt.sign({
            _id: user._id
          }, process.env.RESET_PASSWORD_KEY, {
            expiresIn: "1h"
          });
          data = {
            from: 'noreply@test.com',
            to: email,
            subject: "Password Reset",
            html: "\n            <h2>Request for password reset has been received for this account</h2>\n            <a href=\"".concat(process.env.CLIENT_URL, "/reset-password/").concat(token, "\"> Please click on this link to continue </a>\n            ")
          };
          _context3.next = 10;
          return regeneratorRuntime.awrap(mg.messages().send(data));

        case 10:
          msg = _context3.sent;

          if (msg) {
            _context3.next = 15;
            break;
          }

          return _context3.abrupt("return", {
            message: msg.message
          });

        case 15:
          return _context3.abrupt("return", {
            message: "Reset email has been sent successfully."
          });

        case 16:
        case "end":
          return _context3.stop();
      }
    }
  });
}

function updateUserInfo(userObject, res) {
  var salt, pass, userObjectItem, user, token;
  return regeneratorRuntime.async(function updateUserInfo$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap(bcrypt.genSalt(10));

        case 2:
          salt = _context4.sent;
          _context4.next = 5;
          return regeneratorRuntime.awrap(bcrypt.hash(userObject.password, salt));

        case 5:
          pass = _context4.sent;
          _context4.next = 8;
          return regeneratorRuntime.awrap(User.updateOne({
            _id: userObject._id
          }, {
            $set: {
              username: userObject.username,
              email: userObject.email,
              password: pass,
              role: userObject.role
            }
          }));

        case 8:
          userObjectItem = _context4.sent;
          _context4.next = 11;
          return regeneratorRuntime.awrap(User.findOne({
            email: userObject.email
          }));

        case 11:
          user = _context4.sent;
          token = user.generateAuthToken();
          return _context4.abrupt("return", token);

        case 14:
        case "end":
          return _context4.stop();
      }
    }
  });
}

function resetPassword(req, res) {
  var _req$body = req.body,
      token = _req$body.token,
      password = _req$body.password;

  if (token) {
    return jwt.verify(token, process.env.RESET_PASSWORD_KEY, function _callee2(err, data) {
      var user, salt, newPass, userObj;
      return regeneratorRuntime.async(function _callee2$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              if (!err) {
                _context5.next = 4;
                break;
              }

              return _context5.abrupt("return", {
                status: 401,
                error: "Incorrect or Expired token."
              });

            case 4:
              user = data;
              _context5.next = 7;
              return regeneratorRuntime.awrap(bcrypt.genSalt(10));

            case 7:
              salt = _context5.sent;
              _context5.next = 10;
              return regeneratorRuntime.awrap(bcrypt.hash(password, salt));

            case 10:
              newPass = _context5.sent;
              _context5.next = 13;
              return regeneratorRuntime.awrap(User.updateOne({
                _id: user._id
              }, {
                $set: {
                  password: newPass
                }
              }));

            case 13:
              userObj = _context5.sent;
              return _context5.abrupt("return", {
                status: 200,
                message: "Password has been changed succesfully, please Login"
              });

            case 15:
            case "end":
              return _context5.stop();
          }
        }
      });
    });
  } else {
    return {
      status: 401,
      error: "Authentication error"
    };
  }
}

function verifyToken(req, res) {
  var _req$body2 = req.body,
      type = _req$body2.type,
      token = _req$body2.token;

  if (type === 'reset' && token) {
    return jwt.verify(token, process.env.RESET_PASSWORD_KEY, function (err, data) {
      if (err) {
        // console.log(err);
        // return res.status(401).json({ error: "Incorrect or Expired token." })
        return res.send({
          status: 401,
          error: "Incorrect or Expired token."
        });
      } else {
        return res.send({
          status: 200,
          message: "Link is valid, please type new password"
        });
      }
    });
  } else {
    return res.status(401).json({
      error: "Type is not registered"
    });
  }
}

function validateUser(user) {
  var schema = {
    username: Joi.string().min(3).max(100).required(),
    email: Joi.string().min(8).max(255).required(),
    firstname: Joi.string().min(2).max(255).required(),
    lastname: Joi.string().min(2).max(255).required(),
    password: Joi.string().min(8).max(64).required(),
    teamname: Joi.string().max(100).required(),
    isActivated: Joi["boolean"]().required(),
    role: Joi.number().required()
  };
  return Joi.validate(user, schema);
}

exports.User = User;
exports.activateAccount = activateAccount;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.createUser = createUser;
exports.verifyToken = verifyToken;
exports.updateUserInfo = updateUserInfo;