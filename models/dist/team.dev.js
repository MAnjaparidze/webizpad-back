"use strict";

var mongoose = require('mongoose');

var _require = require('joi'),
    func = _require.func;

var teamSchema = new mongoose.Schema({
  name: String,
  author: String,
  members: Array,
  projects: Array,
  date: {
    type: Date,
    "default": Date.now
  }
});
var Team = mongoose.model("Team", teamSchema);

function createTeam(teamObj) {
  var team, result;
  return regeneratorRuntime.async(function createTeam$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          team = new Team({
            name: teamObj.name,
            author: teamObj.author,
            members: [teamObj.member],
            projects: []
          });
          _context.next = 3;
          return regeneratorRuntime.awrap(team.save());

        case 3:
          result = _context.sent;
          return _context.abrupt("return", result);

        case 5:
        case "end":
          return _context.stop();
      }
    }
  });
}

function getTeam(teamObj) {
  var result;
  return regeneratorRuntime.async(function getTeam$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(Team.findOne({
            name: teamObj.teamname
          }));

        case 2:
          result = _context2.sent;
          console.log(result);
          return _context2.abrupt("return", result);

        case 5:
        case "end":
          return _context2.stop();
      }
    }
  });
}

function addTeamMember(teamObj) {
  var teamItem;
  return regeneratorRuntime.async(function addTeamMember$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          teamItem = Team.findOneAndUpdate({
            _id: teamObj._id
          }, {
            $push: {
              members: teamObj.member
            }
          });
          return _context3.abrupt("return", teamItem);

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
}

function addTeamProject(teamObj) {
  var teamItem;
  return regeneratorRuntime.async(function addTeamProject$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          teamItem = Team.findOneAndUpdate({
            _id: teamObj._id
          }, {
            $push: {
              projects: teamObj.project
            }
          });
          return _context4.abrupt("return", teamItem);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  });
}

exports.Team = Team;
exports.createTeam = createTeam;
exports.getTeam = getTeam;
exports.addTeamMember = addTeamMember;
exports.addTeamProject = addTeamProject;