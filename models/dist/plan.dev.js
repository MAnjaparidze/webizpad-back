"use strict";

var mongoose = require('mongoose');

var _require = require('joi'),
    func = _require.func;

var planSchema = new mongoose.Schema({
  name: String,
  parentID: String,
  testNum: Number,
  passNum: Number,
  failNum: Number,
  queryNum: Number,
  blockedNum: Number,
  excludeNum: Number,
  date: {
    type: Date,
    "default": Date.now
  }
});
var Plan = mongoose.model("Plan", planSchema);

function createPlan(projectPlan) {
  var demoPlan, result, plans;
  return regeneratorRuntime.async(function createPlan$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          demoPlan = new Plan({
            name: projectPlan.name,
            parentID: projectPlan.parentID,
            testNum: projectPlan.testNum,
            passNum: projectPlan.passNum,
            failNum: projectPlan.failNum,
            queryNum: projectPlan.queryNum,
            blockedNum: projectPlan.blockedNum,
            excludeNum: projectPlan.excludeNum
          });
          _context.next = 3;
          return regeneratorRuntime.awrap(demoPlan.save());

        case 3:
          result = _context.sent;
          _context.next = 6;
          return regeneratorRuntime.awrap(Plan.find());

        case 6:
          plans = _context.sent;
          return _context.abrupt("return", plans);

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
}

function getPlans(project) {
  var plans;
  return regeneratorRuntime.async(function getPlans$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(Plan.find().then(function (res) {
            return res.filter(function (c) {
              return c.parentID === project.parentID;
            });
          }));

        case 2:
          plans = _context2.sent;
          return _context2.abrupt("return", plans);

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  });
}

function getPlan(planID) {
  var planItem;
  return regeneratorRuntime.async(function getPlan$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(Plan.findById(planID));

        case 2:
          planItem = _context3.sent;
          return _context3.abrupt("return", planItem);

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
}

exports.Plan = Plan;
exports.createPlan = createPlan;
exports.getPlans = getPlans;
exports.getPlan = getPlan;