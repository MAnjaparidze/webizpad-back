const mongoose = require('mongoose');
const { func } = require('joi');

const { CaseRun } = require('./caseRun');

const caseRunItemSchema = new mongoose.Schema({
    parentStep: String,
    parentRun: String,
    pass: Boolean,
    fail: Boolean,
    query: Boolean,
    block: Boolean,
    exclude: Boolean,
    comment: String,
    link: String,
    date: { type: Date, default: Date.now }
});

const CaseRunItem = mongoose.model("CaseRunItem", caseRunItemSchema);

async function createCaseRunItem(caseRunItem) {
    const demoCaseRunItem = new CaseRunItem({
        parentStep: caseRunItem.parentStep,
        parentRun: caseRunItem.parentRun,
        pass: caseRunItem.pass,
        fail: caseRunItem.fail,
        query: caseRunItem.query,
        block: caseRunItem.block,
        exclude: caseRunItem.exclude,
        comment: caseRunItem.comment,
        link: caseRunItem.link
    });

    const result = await demoCaseRunItem.save();
    const demoCaseRunItems = await CaseRunItem.find();
    return demoCaseRunItems;
}

async function getCaseRunItems(runID) {
    const caseRunItems = await CaseRunItem.find()
        .then(res => {
            return res.filter(c => c.parentRun === runID.id)
        })
    return caseRunItems;
}

async function getCaseRunItem(caseRunItemID) {
    const caseRunItem = await CaseRunItem.findById(caseRunItemID);
    return caseRunItem;
}

async function updateCaseRunItem(caseRunItemObj) {
    const caseRunItem = await CaseRunItem.updateOne({ _id: caseRunItemObj._id }, {
        $set: {
            pass: caseRunItemObj.pass,
            fail: caseRunItemObj.fail,
            query: caseRunItemObj.query,
            block: caseRunItemObj.block,
            exclude: caseRunItemObj.exclude,
            comment: caseRunItemObj.comment,
            link: caseRunItemObj.link
        }
    }) 
    return caseRunItem;
}


exports.CaseRunItem = CaseRunItem;
exports.createCaseRunItem = createCaseRunItem;
exports.getCaseRunItems = getCaseRunItems;
exports.getCaseRunItem = getCaseRunItem;
exports.updateCaseRunItem = updateCaseRunItem;