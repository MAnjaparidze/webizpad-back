const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcrypt');

const { createTeam } = require('../models/team');

// MAILGUN
const mailgun = require("mailgun-js");
const DOMAIN = 'sandboxda2224b9858d4011ab1957ffb9895e26.mailgun.org';
const mg = mailgun({ apiKey: process.env.MAILGUN_APIKEY, domain: DOMAIN });

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 100,
    },
    firstname: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 250
    },
    lastname: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 250
    },
    email: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 64,
    },
    teamname: {
        type: String,
        required: true,
        maxlength: 100
    },
    isActivated: {
        type: Boolean,
        required: true
    },
    role: {
        type: Number,
        required: true
    },
    date: { type: Date, default: Date.now }
})

userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id, _email: this.email, _username: this.username, _firstname: this.firstname, _lastname: this.lastname, _team: this.teamname, _isActivated: this.isActivated, _role: this.role, _date: this.date }, config.get('jwtPrivateKey'));
    return token;
}

const User = mongoose.model('User', userSchema);

async function createUser(req, res) {
    const { error } = validateUser(req.body.user);
    if (error){
        return res.status(400).send(error.details[0].message);
    } 
        

    let user = await User.findOne({ email: req.body.user.email });
    if (user) return res.status(400).send('User already registered.');
    user = new User(_.pick(req.body.user, ['username', 'firstname', 'lastname', 'email', 'password', 'teamname', "isActivated", "role"]))
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    if(!req.body.isTeamMember) {
        let teamObj = {
            name: user.teamname,
            author: user._id,
            member: {
                id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                role: user.role,
                email: user.email,
                date: user.date
            }
        }
        let res = await createTeam(teamObj);
    }
    const token = user.generateAuthToken();

    const tokenAcc = jwt.sign({ _id: user._id }, process.env.JWT_ACC_ACTIVATE, { expiresIn: "20m" });

    const data = {
        from: 'webizpad-team@webizpad.com',
        to: req.body.user.email,
        subject: 'Account Activation Link',
        // text: 'Testing some Mailgun awesomness!'
        html: `
        <h2>Please click on given link to activate your account</h2>
        <a href="${process.env.CLIENT_DEV_URL}/authentication/activate/${tokenAcc}">Click Here to Activate Your Account</a>
        `
    };
    mg.messages().send(data, function (error, body) {
        if (error) {
            return res.json({
                message: error.message
            })
        }
        res.header('x-auth-token', token).send(_.pick(user, ['_id', 'username', 'email']));
    });
    return ({ message: "Account has been created, activate it" })
}

function activateAccount(req, res) {
    const { token } = req.body;
    if (token) {
        return jwt.verify(token, process.env.JWT_ACC_ACTIVATE, async function (err, decodedToken) {
            if (err) {
                return res.send({ status: 200, error: "Incorrect of Expired link." })
            }
            const user = decodedToken;
            const userObj = await User.updateOne({ _id: user._id }, {
                $set: {
                    isActivated: true
                }
            })
            return res.send({ status: 200, message: "Account activated, please Login" });
        })
    }
}

async function forgotPassword(req) {
    const { email } = req.body;
    let user = await User.findOne({ email: req.body.email });

    if (!user) {
        return ({ message: "User with this email does not exist." })
    }

    const token = jwt.sign({ _id: user._id }, process.env.RESET_PASSWORD_KEY, { expiresIn: "1h" });
    const data = {
        from: 'noreply@test.com',
        to: email,
        subject: "Password Reset",
        html: `
            <h2>Request for password reset has been received for this account</h2>
            <a href="${process.env.CLIENT_DEV_URL}/reset-password/${token}"> Please click on this link to continue </a>
            `
    };

    let msg = await mg.messages().send(data);
    if (!msg) {
        return ({
            message: msg.message
        })
    } else {
        return ({ message: "Reset email has been sent successfully." })
    }
}

async function updateUserInfo(userObject, res) {
    const salt = await bcrypt.genSalt(10);
    let pass = await bcrypt.hash(userObject.password, salt);
    const userObjectItem = await User.updateOne({ _id: userObject._id }, {
        $set: {
            username: userObject.username,
            email: userObject.email,
            password: pass,
            role: userObject.role
        }
    })
    let user = await User.findOne({ email: userObject.email });
    const token = user.generateAuthToken();
    return token;
}

function resetPassword(req, res) {
    const { token, password } = req.body;
    if (token) {
        return jwt.verify(token, process.env.RESET_PASSWORD_KEY, async function (err, data) {
            if (err) {
                return ({ status: 401, error: "Incorrect or Expired token." })
            } else {
                const user = data;
                const salt = await bcrypt.genSalt(10);
                let newPass = await bcrypt.hash(password, salt);

                const userObj = await User.updateOne({ _id: user._id }, {
                    $set: {
                        password: newPass
                    }
                })
                return ({ status: 200, message: "Password has been changed succesfully, please Login" });
            }
        })
    } else {
        return ({ status: 401, error: "Authentication error" })
    }
}

function verifyToken(req, res) {
    const { type, token } = req.body;
    if ((type === 'reset') && token) {
        return jwt.verify(token, process.env.RESET_PASSWORD_KEY, function (err, data) {
            if (err) {
                // console.log(err);
                // return res.status(401).json({ error: "Incorrect or Expired token." })
                return res.send({ status: 401, error: "Incorrect or Expired token." })
            } else {
                return res.send({ status: 200, message: "Link is valid, please type new password" });
            }
        })
    } else {
        return res.status(401).json({ error: "Type is not registered" })
    }
}

function validateUser(user) {
    const schema = {
        username: Joi.string().min(3).max(100).required(),
        email: Joi.string().min(8).max(255).required(),
        firstname: Joi.string().min(2).max(255).required(),
        lastname: Joi.string().min(2).max(255).required(),
        password: Joi.string().min(8).max(64).required(),
        teamname: Joi.string().max(100).required(),
        isActivated: Joi.boolean().required(),
        role: Joi.number().required()
    };

    return Joi.validate(user, schema)
}

exports.User = User;
exports.activateAccount = activateAccount;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.createUser = createUser;
exports.verifyToken = verifyToken;
exports.updateUserInfo = updateUserInfo; 