FROM node:12-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY package.json ./
RUN npm install

COPY . .


# If you are building your code for production
# RUN npm ci --only=production

ENV PORT=3000
ENV USE_SSL="true"
ENV MONGO_DB="mongodb://webizpad.webiz.ge/webizpad-db"

EXPOSE 80
CMD [ "npm", "run", "start" ]
