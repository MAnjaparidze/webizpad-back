const mongoose = require('mongoose');
const config = require('config');
const fs = require('fs');

const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const express = require('express');
const app = express();

require('./middleware/prod')(app);
require('dotenv').config();

const users = require('./routes/users');
const auth = require('./routes/auth');
const teams = require('./routes/teams');
const projects = require('./routes/projects');
const cases = require('./routes/cases');
const plans = require('./routes/plans');
const caseSteps = require('./routes/caseSteps');
const caseRuns = require('./routes/caseRuns');
const caseRunItems = require('./routes/caseRunItems');

var cors = require('cors')

app.use(cors());



if (!config.get('jwtPrivateKey')) {
    console.log('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
}

// mongoose.connect(config.get("db"))
const mongoUri = process.env.MONGO_DB;

// set mongoose connection options
if(process.env.USE_SSL === "true"){
    var mongoOpt = {
              "ssl": true,
              "sslValidate": true,
              "sslCA": fs.readFileSync('./certs/ca.pem'),
              "sslCert": fs.readFileSync('./certs/mongodb.pem'),
              "sslKey": fs.readFileSync('./certs/mongodb.pem'),
            };
}else{
    var mongoOpt = {}
};


mongoose.connect(mongoUri, mongoOpt)
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...', err));

app.use(express.json());
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/teams', teams);
app.use('/api/projects', projects);
app.use('/api/cases', cases);
app.use('/api/plans', plans);
app.use('/api/caseSteps', caseSteps);
app.use('/api/caseRuns', caseRuns);
app.use('/api/caseRunItems', caseRunItems);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));