"use strict";

var mongoose = require('mongoose');

var config = require('config');

var Joi = require('joi');

Joi.objectId = require('joi-objectid')(Joi);

var express = require('express');

var app = express();

require('./middleware/prod')(app);

require('dotenv').config();

var users = require('./routes/users');

var auth = require('./routes/auth');

var teams = require('./routes/teams');

var projects = require('./routes/projects');

var cases = require('./routes/cases');

var plans = require('./routes/plans');

var caseSteps = require('./routes/caseSteps');

var caseRuns = require('./routes/caseRuns');

var caseRunItems = require('./routes/caseRunItems');

var cors = require('cors');

app.use(cors());

if (!config.get('jwtPrivateKey')) {
  console.log('FATAL ERROR: jwtPrivateKey is not defined.');
  process.exit(1);
} // mongoose.connect(config.get("db"))


mongoose.connect("mongodb://localhost/webizpad-db").then(function () {
  return console.log('Connected to MongoDB...');
})["catch"](function (err) {
  return console.error('Could not connect to MongoDB...', err);
});
app.use(express.json());
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/teams', teams);
app.use('/api/projects', projects);
app.use('/api/cases', cases);
app.use('/api/plans', plans);
app.use('/api/caseSteps', caseSteps);
app.use('/api/caseRuns', caseRuns);
app.use('/api/caseRunItems', caseRunItems);
var port = process.env.PORT || 3000;
app.listen(port, function () {
  return console.log("Listening on port ".concat(port, "..."));
});